import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { WeatherService } from './weather.service';

@Injectable()
export class LocationService implements Resolve<any>{
  constructor(private weatherSer:WeatherService) { };

  resolve(){
    return this.weatherSer.defaultLocation();
  }
}
