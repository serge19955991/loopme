  import { Component, OnInit, ElementRef} from '@angular/core';
  import {WeatherService} from "../weather.service";
  import {FormControl, FormGroup} from "@angular/forms";
  import {Forecast} from "../forecast";
  import * as moment from 'moment';

  declare let d3: any;

  @Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.scss','../../../node_modules/nvd3/build/nv.d3.css']
  })
  export class ForecastComponent implements OnInit {
  el: HTMLElement;

  constructor(private ws:WeatherService,elementRef: ElementRef) {
    this.el = elementRef.nativeElement;


  }

  forecastForm:FormGroup;
  forecast:Forecast[]=[];

  xaxis = [];
  yaxis = [];

  data;
  options;

  ngOnInit() {
    this.forecastForm = new FormGroup({
      forecastCity:new FormControl('')
    });

    this.options = {
      chart:{
        type:'discreteBarChart',
        height:450,
        margin : {
          top: 20,
          right: 20,
          bottom : 50,
          left : 55
        },
        x: function(d){return d.label;},
        y: function(d){return d.value;},
        showValues:true,
        valueFormat: function(d){
          return d3.format(',.4f')(d);
        },
        duration:500,
        xAxis: {
          axisLabel:'Date',

        },
        yAxis: {
          axisLabel:'Temperature',
          axisLabelDistance:-10
        }
      }
    };

  }

  onSubmit(){
    this.xaxis.splice(0,this.xaxis.length);
    this.yaxis.splice(0,this.yaxis.length);

    this.ws.sixteendaysForecast(this.forecastForm.value.forecastCity).subscribe(
      (data) => {
        console.log(data);
        for(let i=0; i < data.list.length; i++){
          const date = moment.unix(data.list[i].dt);
          const t = data.list[i].temp.day;
          const forecastWeather = new Forecast(data.city.name,data.list[i].dt, data.list[i].temp.day);
          this.forecast.push(forecastWeather);
          this.xaxis.push(t);
          this.yaxis.push(date);

        }
          this.data = [{
          key:"Weather Forecast",
          values:[
            {
              "label": this.yaxis[0],
              "value": this.xaxis[0]
            },
            {
              "label": this.yaxis[1],
              "value": this.xaxis[1]
            },
            {
              "label": this.yaxis[2],
              "value": this.xaxis[2]
            },
            {
              "label": this.yaxis[3],
              "value": this.xaxis[3]
            },
            {
              "label": this.yaxis[4],
              "value": this.xaxis[4]
            },
            {
              "label": this.yaxis[5],
              "value": this.xaxis[5]
            },
            {
              "label": this.yaxis[6],
              "value": this.xaxis[6]
            },
            {
              "label": this.yaxis[7],
              "value": this.xaxis[7]
            },
            {
              "label": this.yaxis[8],
              "value": this.xaxis[8]
            },
            {
              "label": this.yaxis[9],
              "value": this.xaxis[9]
            },
            {
              "label": this.yaxis[10],
              "value": this.xaxis[10]
            },
            {
              "label": this.yaxis[11],
              "value": this.xaxis[11]
            },
            {
              "label": this.yaxis[12],
              "value": this.xaxis[12]
            },
            {
              "label": this.yaxis[13],
              "value": this.xaxis[13]
            },
            {
              "label": this.yaxis[14],
              "value": this.xaxis[14]
            },
            {
              "label": this.yaxis[15],
              "value": this.xaxis[15]
            }
          ],

        }];
          let gchange = this.el.getElementsByClassName('nv-x')[0];
          gchange.setAttribute("display","none");
          console.log(this.forecast);
          return this.forecast;

      }

    );

 }

}
