export class Weather {
  constructor (public cityName:string,
               public temp:string,
               public img:string,
               public pressure:string,
               public humidity:string,
               public windSpeed:string,
               public clouds:string){}
}
