export class Forecast {
  constructor (public cityName:string,
               public date: string,
               public temp: string){}
}
