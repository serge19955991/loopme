import { Injectable } from '@angular/core';
import {Weather} from "./weather";
import {Http,Response} from "@angular/http";
import 'rxjs/Rx';

@Injectable()
export class WeatherService {
  private weather:Weather[]= [];
  weatherClass:Weather;
  location;
  constructor(private _http:Http) { }

  defaultLocation(){
    return new Promise((res,rej) => {
      navigator.geolocation.getCurrentPosition((pos) => {
        this.location = pos.coords;
        const lat = this.location.latitude;
        const lon = this.location.longitude;
        console.log(`lat ${lat} and lon ${lon}`);
        return this._http.get(`http://api.openweathermap.org/data/2.5/weather?appid=edb941c294b159c0267b5ff08891bd23&lat=${lat}&lon=${lon}&units=metric`)
          .map((response:Response) => response.json()).toPromise()
          .then(
            (data) => {
              this.weatherClass = new Weather(data.name, data.main.temp, data.weather[0].icon, data.main.pressure, data.main.humidity, data.wind.speed, data.clouds.all);
              res(this.weatherClass);
              return this.weatherClass;
            }
          );
      })
    })
  }

  difWeather(city:string){
    return this._http.get(`http://api.openweathermap.org/data/2.5/weather?appid=edb941c294b159c0267b5ff08891bd23&q=${city}&units=metric&cnt=10`)
      .map((response:Response) => response.json());
  }
  sixteendaysForecast(city:string){
    return this._http.get(`http://api.openweathermap.org/data/2.5/forecast/daily?appid=edb941c294b159c0267b5ff08891bd23&q=${city}&units=metric&cnt=16`)
      .map((response:Response) => response.json());
  }
}
