  import { Component, OnInit } from '@angular/core';
  import {Weather} from "../weather";
  import {WeatherService} from "../weather.service";
  import {ActivatedRoute} from "@angular/router";
  import {NgForm} from "@angular/forms";


  @Component({
    selector: 'app-weather',
    templateUrl: './weather.component.html',
    styleUrls: ['./weather.component.scss']
  })
  export class WeatherComponent implements OnInit {
    weather:Weather;

    constructor(private ws:WeatherService, private route:ActivatedRoute) { }

    weatherClass:Weather;
    location:any = {};
    ngOnInit() {
      this.route.data.subscribe(
        (data:{weatherClass:Weather}) => {
          this.weatherClass = data.weatherClass;
        }
      )
    }

    onSubmit(weatherForm: NgForm){
      this.ws.difWeather(weatherForm.value.city).subscribe(
        (data) => {
          this.weatherClass = new Weather(data.name, data.main.temp, data.weather[0].icon, data.main.pressure, data.main.humidity, data.wind.speed, data.clouds.all);
          console.log(this.weatherClass);
        }
      )
    }

  }
