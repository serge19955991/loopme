import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NvD3Module } from 'ng2-nvd3';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ForecastComponent } from './forecast/forecast.component';
import { WeatherComponent } from './weather/weather.component';
import {weatherRouting} from "./weather.routing";
import {WeatherService} from "./weather.service";
import {LocationService} from "./location.service";
import {HttpModule} from "@angular/http";

import 'd3';
import 'nvd3';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ForecastComponent,
    WeatherComponent
  ],
  imports: [
    BrowserModule,
    weatherRouting,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    NvD3Module
  ],
  providers: [WeatherService,LocationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
